#!/usr/bin/env python
#
# Simple script showing how to read a mitmproxy dump file
#

from mitmproxy import flow
import pprint
import sys

with open(sys.argv[1], "rb") as logfile:
    freader = flow.FlowReader(logfile)
    pp = pprint.PrettyPrinter(indent=4)
    try:
        for f in freader.stream():
            print(f.request.host)
            print(dir(f.request))
            pp.pprint(f.get_state())
            print("")
    except FlowReadException as e:
        print("Flow file corrupted: {}".format(e))