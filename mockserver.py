#!/usr/bin/python
import re
import os
import xml.dom.minidom
from BaseHTTPServer import BaseHTTPRequestHandler,HTTPServer
from distutils.dir_util import copy_tree

PORT_NUMBER = 3218


class myHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        print "Recieved GET request"
        try:
            print self.rfile.read(int(self.headers.getheader('Content-Length')))
            print self.headers.getheader('cookie')
        except:
            print "No Input Data"
        self.send_response(200)
        self.send_header('Content-type','text/xml')
        self.end_headers()
        self.wfile.write('<H1>Hello World</H1>')
        return

    def do_POST(self):
        requestingHost=self.headers.getheader('Host')
        requestingPath=self.path
        print "Recieved POST request from %s-%s"%(requestingHost,requestingPath)
        try:
            if not self.headers.getheader('Content-Length'):#Fix for requests without Content-Length
                self.rfile.readline()
                incomingRequest = self.rfile.readline()
                mockeloMockId=re.search("PyUnitsMockId[0-9]+",str(incomingRequest)).group(0)
            else:
                contentLength = int(self.headers.getheader('Content-Length'))
                incomingRequest = self.rfile.read(contentLength)
                mockeloMockId=str(xml.dom.minidom.parseString(incomingRequest).getElementsByTagName('trackingId')[0].firstChild.nodeValue)
        except:
                print "Could not find MockId from Request"
        try:
                mockeloMockIdSwapSpace = mockeloMockId+"SwapSpace"
                if not os.path.exists(mockeloMockIdSwapSpace):
                    os.mkdir(mockeloMockIdSwapSpace)
                    copy_tree('mockelos//%s'%mockeloMockId, mockeloMockIdSwapSpace)
                self.send_response(200)
                self.send_header('Content-type','text/xml')
                self.end_headers()
                mockeloMockPath=requestingPath.replace("http://"+requestingHost,"").replace("?wsdl","")
                with open('%s/%s/POST/response'%(mockeloMockIdSwapSpace,mockeloMockPath), 'r') as fifoDumpBefore:
                        fifoResponse = fifoDumpBefore.readline()
                        remainingDump = fifoDumpBefore.read()
                with open('%s/%s/POST/response'%(mockeloMockIdSwapSpace,mockeloMockPath), 'w') as fifoDumpAfter:
                        fifoDumpAfter.writelines(remainingDump)
                self.wfile.write(fifoResponse)
                print 'Responded with Mock - %s/%s/POST/response'%(mockeloMockIdSwapSpace,mockeloMockPath)
        except:
                print "Something went wrong"
        return

try:
    server = HTTPServer(('', PORT_NUMBER), myHandler)
    print 'Started httpserver on port ' , PORT_NUMBER

    #Wait forever for incoming http requests
    server.serve_forever()

except KeyboardInterrupt:
    print '^C received, cleaning project and shutting down the server'
    os.system("rm -rf *SwapSpace")
    server.socket.close()