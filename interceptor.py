import os
import signal
import subprocess

PORT=3218
dumpfile="dumpfile"
workingDirectory="/Users/lingsy/app/mockelos/"
workingDirectory="/home/laradh001c/mockelo/"
mitmCommand="mitmdump -s %ssslmanager.py -p %d -w %s%s"%(workingDirectory,PORT,workingDirectory,dumpfile)

class ForkNewProcess(object):
    '''Starts a new Process on your OS (from where you are running this Script from)
       Pass the .exe or /.sh to the init method
       Example: com = ForkNewProcess("notepad").run()'''

    def __init__(self, command, shell=True):
        self.command = command
        self.shell = shell

    def run(self):
        process = subprocess.Popen(self.command, shell=self.shell,stdout=subprocess.PIPE, stderr=subprocess.PIPE, preexec_fn=os.setsid)
        self.output, self.error = process.communicate()
        self.returnCode = process.returncode
        return self

    def getReturnCode(self):
        return self.returnCode

    def getOutput(self):
        return self.output

    def getError(self):
        return self.error

if __name__ == '__main__':
  fp = ForkNewProcess(mitmCommand)
  print fp.run()
  print fp.getOutput()
  print fp.getReturnCode()
  print fp.getError()