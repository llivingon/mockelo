#!/usr/bin/env python
SUT_HOST='uivrdas-wc-a01q.sys.comcast.net'

from mitmproxy import flow
import pprint
import sys
import os

#Verify Argv
try:
        print('Generating mocks using Dump File --->', sys.argv[1])
except Exception:
        print ('Try running the program with a dumpfile. Ex: python impedanceAlgorithm.py dumpfileName')
        sys.exit()

#Create Unique Mock Repository
with open(sys.argv[1], "rb") as networkDump:
    freader = flow.FlowReader(networkDump)
    for f in freader.stream():
        if f.request.host == SUT_HOST:
            uniqueJSessionID= str(f.request.cookies['JSESSIONID'][0])

#Identify and Create Mocks
with open(sys.argv[1], "rb") as networkDump:
    freader = flow.FlowReader(networkDump)
    pp = pprint.PrettyPrinter(indent=4)
    try:
        for f in freader.stream():
            if f.request.host !=SUT_HOST:
                path='mockelos/%s/%s/%s'%(uniqueJSessionID,f.request.path,f.request.method)
                path = path.replace('?wsdl','')
                if not os.path.exists(path):
                        os.system('mkdir -p %s'%path)
                targetrequest = open('%s/request'%path, 'a')
                targetrequest.write(str(f.get_state()['request']['content'])+"\n")
                targetrequest.close()
                targetresponse = open('%s/response'%path, 'a')
                targetresponse.write(str(f.get_state()['response']['content'])+"\n")
                targetresponse.close()
                print(f.request.path)
                print(f.request.method)
                print("")
    except FlowReadException as e:
        print("Flow file corrupted: {}".format(e))